function H=hamiltonian(q,p)
  H=(q^2+p^2)/2; % This is the Hamiltonian of the harmonic oscillator
  end
