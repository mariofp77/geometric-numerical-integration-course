function [H,traj]=main(n,h,q,p)
  % n: number of steps
  % h: time step
  % q: initial position
  % p: initial momenta
  % A possible informative example is n= 4, h = 1.85, q = 1, p = 0
  
  H=[];
  traj=zeros(n+1,2); % We initialize the trajectory with n+1 rows because the first one is for the initial conditions
  
  % We define half step
  h2=h/2;
  
  % We initialize both Hamiltonian and trajectory
  H(1)=hamiltonian(q,p);
  traj(1,:)=[q p];
  
  % Main integration loop
  for i=1:n
    p=mom_int(h2,q,p); % Half step for momenta
    q=pos_int(h,q,p); % One step for positions
    p=mom_int(h2,q,p); % Half step for momenta
    
    % We compute the Hamiltonian
    H(i+1)=hamiltonian(q,p);
    % We store the positions and momenta in the trajectory
    traj(i+1,:)=[q,p];
  end
  
  % The generated points are plotted
  plot(traj(:,1),traj(:,2),'*r')
  hold on
  ezplot(@(x,y)(x.^2+y.^2)/2-H(1)) % The corresponding level set of the true Hamiltonian
  end